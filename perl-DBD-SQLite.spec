Name:           perl-DBD-SQLite
Version:        1.76
Release:        1
Summary:        DBD::SQLite embeds SQLite database engine into a DBD driver
License:        (GPL-1.0-or-later or Artistic-1.0) and Public Domain
URL:            https://metacpan.org/release/DBD-SQLite
Source0:        https://cpan.metacpan.org/authors/id/I/IS/ISHIGAKI/DBD-SQLite-%{version}.tar.gz

BuildRequires:  sqlite-devel coreutils findutils
BuildRequires:  gcc make perl-devel
BuildRequires:  perl-generators perl-interpreter perl(:VERSION) >= 5.6
BuildRequires:  perl(base) perl(Config) perl(constant)
BuildRequires:  perl(DBI) >= 1.607 perl(DBI::DBD) perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(File::Spec) >= 0.82 perl(strict)

BuildRequires:  perl(DynaLoader) perl(locale) perl(Scalar::Util)
BuildRequires:  perl(Tie::Hash) perl(warnings) sed

BuildRequires:  perl(bytes) perl(Carp) perl(Data::Dumper)
BuildRequires:  perl(Encode) perl(Exporter) perl(File::Spec::Functions)
BuildRequires:  perl(File::Temp) perl(FindBin) perl(lib)

BuildRequires:  perl(Test::More) perl(Test::NoWarnings) >= 0.081
BuildRequires:  perl(Unicode::UCD)

Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))

%{?perl_default_filter}

%description
SQLite is a software library that implements a self-sufficient, serverless,
zero-configuration, transactional SQL database engine.

SQLite can be integrated with Perl using the Perl DBI module.
The Perl DBI module is the database access module for the Perl programming language.

%package        help
Summary:        Including man files for perl-DBD-SQLite
Requires:       man

%description    help
This contains man files for the using of perl-DBD-SQLite.


%prep
%autosetup -n DBD-SQLite-%{version} -p1

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build OPTIMIZE="%{optflags}"

%install
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -name '*.bs' -type f -delete
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/DBD/

%files help
%{_mandir}/man3/*.3pm*

%changelog
* Mon Nov 4 2024 zhangxianjun <zhangxianjun@kylinos.cn> - 1.76-1
- update to 1.76
- Switched to a production version
- Upgraded SQLite to 3.46.1
- Fix for Windows quadmath builds (GH#115, sisyphus++)
- Omit load_extension if static build

* Tue Mar 19 2024 zhangxianjun <zhangxianjun@kylinos.cn> - 1.74-1
- update to 1.74

* Thu Nov 17 2022 dillon chen <dillon.chen@gmail.com> - 1.72-1
- update to 1.72

* Sat Jun 25 2022 Xinyi Gou <plerks@163.com> - 1.70-2
- Specify license version

* Thu Dec 9 2021 yanglongkang <yanglongkang@huawei.com> - 1.70-1
- update package to 1.70

* Thu Jan 14 2021 yanglongkang <yanglongkang@huawei.com> - 1.66-1
- update package to 1.66

* Fri Jul 17 2020 yanglongkang <yanglongkang@huawei.com> - 1.65_02-1
- update package to 1.65_02

* Wed Jul 1 2020 Wu Bo <wubo009@163.com> - 1.58-5
- rebuild package

* Sat Aug 31 2019 zhangsaisai<zhangsaisai@huawei.com> - 1.58-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:openEuler Debranding.

* Wed Aug 21 2019 guiyao<guiyao@huawei.com> - 1.58-3.h2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:change patchs' name

* Tue Apr 16 2019 wangxiao<wangxiao65@huawei.com> - 1.58-3.h1
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:Handle unknown op in DBD SQLite VirtualTable PerlData
       added a boilerplace for test
       fixed for older DBI
- Package init
